<!DOCTYPE html>
<html>
<head>
	 <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	<link href="https://fonts.googleapis.com/css?family=Slabo 27px" rel="stylesheet">
	<title></title>
</head>
<body>
	<div class="container">
		<form>
			
			<div class="row">
				<div class="col-6">
					<div class="form-group">
						<label>Titulo</label>
						<input class="form-control text-uppercase" type="text" maxlength="16" name="des_numero" required>
					</div>		
				</div>
				<div class="col-6">
					<div class="form-group">
						<label>Presentacion Pantalla Inicio</label>
						<select class="form-control  text-uppercase custom-select" name="empresa_id" required>
							<option value="0">Solo texto sin imagen</option>
							<option value="1">Imagen lado izquierdo, texto derecho</option>
							<option value="2">Texto lado izquierdo, imagen derecho</option>
						</select>
					</div>
				</div>
				<div class="col-6">
					<div class="form-group">
						<label>Presentacion Pantalla Propia</label>
						<select class="form-control  text-uppercase custom-select" name="empresa_id" required>
							<option value="0">Solo texto sin imagen</option>
							<option value="1">Imagen al Inicio Centrada</option>
							<option value="2">Imagen al Inicio lado Izquierdo</option>
							<option value="3">Imagen al Inicio lado Derecho</option>
							<option value="4">Imagen al Pie centrado</option>
						</select>
					</div>
				</div>
				<div class="col-12">
					<div class="form-group">
						<label>Texto</label>
						<textarea class="form-control text-uppercase" rows="3" name="des_descripcion_especifica" required></textarea>
					</div>
				</div>
				<div class="col-12">
					<div class="custom-control custom-radio custom-control-inline">
						<input type="radio" id="imagen_1" name="imagen_1" class="custom-control-input">
						<label class="custom-control-label" for="imagen_1">Una Imagen</label>
					</div>
					<div class="custom-control custom-radio custom-control-inline">
						<input type="radio" id="imgaen_2" name="imagen_1" class="custom-control-input">
						<label class="custom-control-label" for="imgaen_2">Varias Imagen</label>
					</div>
				</div>	
				<div class="col-12">
					<div class="custom-file">
						<input type="file" class="custom-file-input" id="customFile">
						<label class="custom-file-label" for="customFile">Seleccione Imagen</label>
					</div>	
				</div>	
			</div>
			<div class="row">
				<div class="col text-center">
					<button id="guardar" type="submit" class="btn btn-primary mb-2" disabled>Guardar</button>
				</div>
			</div>
		</form>
	</div>

	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</body>
</html>