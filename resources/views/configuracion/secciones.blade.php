@extends('config')

@section('contenido')
<div class="container">
	<div class="row">
		<div class="col align-self-center text-center">
			<h4>Nueva Seccion</h4>
		</div>
	</div>
	<form>
		<div class="form-group">
			<label for="nombre">Nombre</label>
			<input type="text" class="form-control" id="nombre">
		</div>
		<div class="custom-file">
		  <input type="file" class="custom-file-input" id="customFile">
		  <label class="custom-file-label" for="customFile">Seleccione el Logotipo</label>
		</div>
		<div class="form-group">
			<label>Color de Fondo</label>
			<input class="form-control text-uppercase" type="color" value="#d1d293" name="des_numero" required>
		</div>
		<div class="form-group">
			<label>Boton Seccion</label>
			<input class="form-control text-uppercase" type="color" value="#3a3f44" name="des_numero" required>
		</div>
		<div class="form-group">
			<label>Subsecciones y contenido</label>
			<input class="form-control text-uppercase" type="color" value="#ffffff" name="des_numero" required>
		</div>

		<button type="submit" class="btn btn-primary">Guardar</button>
</form>
<div class="container" style="background-color: #d1d293">
<div class="row">
	<div class="col-12">
		<div class="row">
			<div class="col-4 offset-4 mt-1" style="height: 60px; background-color: #3a3f44">
				Nombre de Seccion
			</div>
		</div>
		<div class="row">
			<div class="col-12 mt-1" style="height: 60px; background-color: #000000">
				Logo Tipo
			</div>
		</div>
		<div class="row">
			<div class="col-12 mt-1" style="height: 60px; background-color: #ffffff">
				Subsecciones
			</div>
		</div>
		<div class="row">
			<div class="col-12 mt-1" style="height: 60px; background-color: #ffffff">
				Contenido
			</div>
		</div>
	</div>
</div>
	
</div>
</div>
@stop