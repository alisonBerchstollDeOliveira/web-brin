@extends('test')

@section('estado-1')
    show active
@stop

@section('1')
    <div class="row">
        <div class="col-12  text-center" style="background: rgb(65,176,177);background: linear-gradient(0deg, rgba(65,176,177,1) 0%, rgba(45,253,163,1) 100%);">
            <img src="/img/test.png" class="img-fluid" alt="Responsive image">
        </div>
        <div class="col-12 shadow-sm p-2 mb-1 bg-white bg-light">
            <nav class="nav">
                <a class="nav-link active text-muted" href="/">Inicio</a>
                <a class="nav-link text-muted" href="@yield('direccion-1')">Informacion</a>
                <a class="nav-link text-muted" href="@yield('direccion-2')">Servicio</a>
                <a class="nav-link text-muted" href="@yield('direccion-3')">Contacto</a>
            </nav>
        </div>
        
        <!------------------------------------------------------------------------------------------->
            @yield('contenido')
        <!------------------------------------------------------------------------------------------->
    </div>

@stop


@section('2')
    <div class="row">
        <div class="col-6 offset-3 text-center shadow rounded" style="margin-top: 100px; height: 200px; background-color: #007bff">
            <h3 style="margin-top: 83px; color: white">Em Breve....</h3>
        </div>
    </div>
@stop


@section('3')
    <div class="row">
        <div class="col-6 offset-3 text-center shadow rounded" style="margin-top: 100px; height: 200px; background-color: #000000">
            <h3 style="margin-top: 83px; color: white">Em Breve....</h3>
        </div>
        <div class="col-12"></div>
    </div>
@stop