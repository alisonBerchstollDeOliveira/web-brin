<!DOCTYPE html>
<html>
<head>
	<!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	<link href="https://fonts.googleapis.com/css?family=Slabo 27px" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="/css/owl.carousel.min.css">
	<link rel="stylesheet" type="text/css" href="/css/owl.theme.default.min.css">
	<style type="text/css">
	    a{
	      color: #8d88b9;
	    }
	    .footer {
	      position: absolute;
	      bottom: 0;
	      width: 100%;
	      height: 60px;
	      line-height: 60px;
	      background-color: #f5f5f5;
	    }
	    html {
	      position: relative;
	      min-height: 100%;
	    }
	    body {
	      margin-bottom: 65px;
	    }
	  </style>
	<title>Inicio</title>
</head>
<body style="background-color: #ececec!important">

	<div class="container-fluid">
		<!--Seccion-->
		<div class="row">
			<div class="col-12" style="padding-right: 0px;padding-left: 0px;">
				<ul class="nav nav-pills nav-fill justify-content-center" id="pills-tab" role="tablist">
					<li class="nav-item">
						<a class="nav-link" id="pills-home-tab" data-toggle="pill" href="#pills-home" role="tab" aria-controls="pills-home" aria-selected="true" style="border-radius: 0; color: #000;background-color: #2efaa3;"><strong>Despachos</strong></a>
					</li>
					<li class="nav-item">
						<a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-profile" role="tab" aria-controls="pills-profile" aria-selected="false" style="border-radius: 0; color: #000; background-color: #007bff;"><strong>Tes</strong></a>
					</li>
					<li class="nav-item">
						<a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#pills-contact" role="tab" aria-controls="pills-contact" aria-selected="false" style="border-radius: 0; background-color: #000000; color: white;"><strong>Juridico</strong></a>
					</li>
				</ul>
				
			</div>
		</div>

		<div class="tab-content" id="pills-tabContent">
			<!--Subseccion y contenido-->
			<div class="tab-pane fade @yield('estado-1')" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
    			@yield('1')
				
			</div>
			<!--Subseccion y contenido-->	

			<div class="tab-pane fade @yield('estado-2')" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
    			@yield('2')
				
			</div>
			<!--Subseccion y contenido-->	
			<div class="tab-pane fade @yield('estado-3')" id="pills-contact" role="tabpanel" aria-labelledby="pills-contact-tab">
    			@yield('3')
				
			</div>
		</div>
	</div>
	<footer class="footer" style="background-color: #343a40;">
    <!-- Copyright -->
    <div class="container text-center" style="color: white;">© 2019 Copyright - Despachos San Miguel</div>
    <!-- Copyright -->

  </footer>

	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
	<script type="text/javascript" src="/js/owl.carousel.min.js"></script>
	<script type="text/javascript">
		$('.owl-carousel').owlCarousel({
			items:4,
			loop:true,
			margin:10,
			autoplay:true,
			autoplayTimeout:2000,
			autoplayHoverPause:false
		})
	</script>

</body>
</html>