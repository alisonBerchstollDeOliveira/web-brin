@extends('test')

@section('estado-1')
    @if ($a == 1)
        show active
    @endif
@stop

@section('estado-2')
    @if ($a == 2)
        show active
    @endif
@stop

@section('estado-3')
    @if ($a == 3)
        show active
    @endif
@stop

@section('1')
    <div class="row">
        <div class="col-12  text-center" style="background: rgb(65,176,177);background: linear-gradient(0deg, rgba(65,176,177,1) 0%, rgba(45,253,163,1) 100%);">
            <img src="/img/test.png" class="img-fluid" alt="Responsive image">
        </div>
        <div class="col-12 shadow-sm p-2 mb-1 bg-white bg-light">
            <nav class="nav">
                <a class="nav-link active text-muted" href="">Inicio</a>
                <a class="nav-link text-muted" href="/informacion">Informacion</a>
                <a class="nav-link text-muted" href="#">Servicio</a>
                <a class="nav-link text-muted" href="#">Contacto</a>
            </nav>
        </div>
        
        <!------------------------------------------------------------------------------------------->

        <div class="col-12 shadow p-3 mb-1 bg-white bg-light" style="background-color: whitesmoke; margin-top: 5px; margin-bottom: 5px;">
            <div class="row">
                <div class="col-9">
                    <div class="row">
                        <div class="col-12" style="font-family: 'Slabo 27px', sans-serif; font-size: 30px;">
                            Informacion
                        </div>
                        <div class="col-12">
                            <p class="text-justify">
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                                consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                                cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non las cosa son muy lokas en un tiempo espacio diferente donde nace las posibilidades de pensar en cosa inexistente dando origen a teorias muchos lokas fuente d***** sinteticas porque lo natural hace mal... <a href="">Leer mas.</a>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-3">
                    <img src="https://www.cocacolaespana.es/content/dam/journey/es/es/private/historia/love-coca-cola/2015/logo-coca-cola-lead.png" style="width: 100%">
                </div>
            </div>
        </div>
        
        <!------------------------------------------------------------------------------------------->
        
        <div class="col-12 shadow p-3 mb-1 bg-white bg-light" style="background-color: whitesmoke; margin-top: 5px; margin-bottom: 5px;">
            <div class="row">
                <div class="col-3">
                    <img src="https://www.cocacolaespana.es/content/dam/journey/es/es/private/historia/love-coca-cola/2015/logo-coca-cola-lead.png" style="width: 100%">
                </div>
                <div class="col-9">
                    <div class="row">
                        <div class="col-12" style="font-family: 'Slabo 27px', sans-serif; font-size: 30px;">
                            Servicios
                        </div>
                        <div class="col-12">
                            <p class="text-justify text-truncate">
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                                consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                                cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                                proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                                consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                                cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                                proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <!------------------------------------------------------------------------------------------->
        
        <div class="col-12 shadow p-3 mb-1 bg-white bg-light" style="background-color: whitesmoke; margin-top: 5px; margin-bottom: 5px;">
            <div class="row">
                <div class="col-12">
                    <div class="row">
                        <div class="col-12" style="font-family: 'Slabo 27px', sans-serif; font-size: 30px;">
                            Contacto
                        </div>
                        <div class="col-12">
                            <p class="text-justify text-truncate">
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                                consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                                cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                                proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                                consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                                cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                                proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <!------------------------------------------------------------------------------------------->
        
        <div class="col-12 shadow p-3 mb-1 bg-white bg-light" style="background-color: whitesmoke; margin-top: 5px; margin-bottom: 5px;">
            <div class="row">
                <div class="col-12">
                    <div class="row">
                        <div class="col-12" style="font-family: 'Slabo 27px', sans-serif; font-size: 30px;">
                            Parceros
                        </div>
                        <div class="col-12">
                            <div class="owl-carousel owl-theme">
                                <div class="item"><img src="https://www.cocacolaespana.es/content/dam/journey/es/es/private/historia/love-coca-cola/2015/logo-coca-cola-lead.png" width="100%" height="100%"></div>
                                <div class="item"><a href="google.com"><img src="https://www.cocacolaespana.es/content/dam/journey/es/es/private/historia/love-coca-cola/2015/logo-coca-cola-lead.png" width="100%" height="100%"></a></div>
                                <div class="item"><img src="https://www.cocacolaespana.es/content/dam/journey/es/es/private/historia/love-coca-cola/2015/logo-coca-cola-lead.png" width="100%" height="100%"></div>
                                <div class="item"><img src="https://www.cocacolaespana.es/content/dam/journey/es/es/private/historia/love-coca-cola/2015/logo-coca-cola-lead.png" width="100%" height="100%"></div>
                                <div class="item"><img src="https://www.cocacolaespana.es/content/dam/journey/es/es/private/historia/love-coca-cola/2015/logo-coca-cola-lead.png" width="100%" height="100%"></div>
                                <div class="item"><img src="https://www.cocacolaespana.es/content/dam/journey/es/es/private/historia/love-coca-cola/2015/logo-coca-cola-lead.png" width="100%" height="100%"></div>
                                <div class="item"><img src="https://www.cocacolaespana.es/content/dam/journey/es/es/private/historia/love-coca-cola/2015/logo-coca-cola-lead.png" width="100%" height="100%"></div>
                                <div class="item"><img src="https://www.cocacolaespana.es/content/dam/journey/es/es/private/historia/love-coca-cola/2015/logo-coca-cola-lead.png" width="100%" height="100%"></div>
                                <div class="item"><img src="https://www.cocacolaespana.es/content/dam/journey/es/es/private/historia/love-coca-cola/2015/logo-coca-cola-lead.png" width="100%" height="100%"></div>
                                <div class="item"><img src="https://www.cocacolaespana.es/content/dam/journey/es/es/private/historia/love-coca-cola/2015/logo-coca-cola-lead.png" width="100%" height="100%"></div>
                                <div class="item"><img src="https://www.cocacolaespana.es/content/dam/journey/es/es/private/historia/love-coca-cola/2015/logo-coca-cola-lead.png" width="100%" height="100%"></div>
                                <div class="item"><img src="https://www.cocacolaespana.es/content/dam/journey/es/es/private/historia/love-coca-cola/2015/logo-coca-cola-lead.png" width="100%" height="100%"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <!------------------------------------------------------------------------------------------->
        
        <div class="col-12 shadow p-3 mb-1 bg-white bg-light" style="background-color: whitesmoke; margin-top: 5px; margin-bottom: 5px;">
            <div class="media">
              <img src="https://www.cocacolaespana.es/content/dam/journey/es/es/private/historia/love-coca-cola/2015/logo-coca-cola-lead.png" class="mr-3 align-self-start" alt="..." style="width: 200px;">
              <div class="media-body">
                <h5 class="mt-0">Media heading</h5>
                Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. Donec lacinia congue felis in faucibus.
              </div>
            </div>
        </div>
       
        <!------------------------------------------------------------------------------------------->


        <div class="col-12 shadow p-3 mb-1 bg-white bg-light" style="background-color: whitesmoke; margin-top: 5px; margin-bottom: 5px;">
            <div class="row">
                <div class="col-3">
                    <div class="p-1 shadow-sm text-center rounded">
                        <div class="row">
                            <div class="col-12">
                              <h4>Importacion</h4>  
                            </div>
                            <div class="col-12 mt-1">
                                <p class="text-justify">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                                consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                                cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                                proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-3">
                    <div class="p-1 shadow-sm text-center rounded">
                        <div class="row">
                            <div class="col-12">
                                <h4>Exportacion</h4>
                            </div>
                            <div class="col-12 mt-1">
                                <p class="text-justify">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                                consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                                cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                                proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-3">
                    <div class="p-1 shadow-sm text-center rounded" style="height: 100%">
                        <div class="row">
                            <div class="col-12">
                                <h4>Otro</h4>
                            </div>
                            <div class="col-12 mt-1">
                                <p>Es otra Cosa</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-3">
                    <div class="p-1 shadow-sm text-center rounded" style="height: 100%">
                        <div class="row">
                            <div class="col-12">
                                <h4>Otro</h4>
                            </div>
                            <div class="col-12 mt-1">
                                <p>El mismo de otra Cosa</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!------------------------------------------------------------------------------------------->

    </div>

@stop


@section('2')
    <div class="row">
        <div class="col-6 offset-3 text-center shadow rounded" style="margin-top: 100px; height: 200px; background-color: #007bff">
            <h3 style="margin-top: 83px; color: white">Em Breve....</h3>
        </div>
    </div>
@stop


@section('3')
    <div class="row">
        <div class="col-6 offset-3 text-center shadow rounded" style="margin-top: 100px; height: 200px; background-color: #000000">
            <h3 style="margin-top: 83px; color: white">Em Breve....</h3>
        </div>
        <div class="col-12"></div>
    </div>
@stop