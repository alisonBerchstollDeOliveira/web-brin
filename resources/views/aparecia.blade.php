<!DOCTYPE html>
<html>
<head>
	<!-- Compiled and minified CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">

    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
	<title></title>
</head>
<body>
	<div class="container">
		<form>
			<div class="row">
				<div class="col-12">
					<div class="form-group">
						<label>Color de Letras</label>
						<input class="form-control text-uppercase" type="color" value="#000000" name="des_numero" required>
					</div>
				</div>
				<div class="col-12">
					<div class="form-group">
						<label>Color de Fondo</label>
						<input class="form-control text-uppercase" type="color" value="#000000" name="des_numero" required>
					</div>
				</div>
				<div class="col-12">
					<div class="form-group">
						<label>Color de Cuadro de Presentacion</label>
						<input class="form-control text-uppercase" type="color" value="#000000" name="des_numero" required>
					</div>
				</div>
			</div>
		</form>
	</div>
	  <!-- Compiled and minified JavaScript -->
      <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
    </script>
</body>
</html>