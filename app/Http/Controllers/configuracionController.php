<?php

namespace web\Http\Controllers;

use Illuminate\Http\Request;

class configuracionController extends Controller
{
    public function pantalla_1(){
    	return view('inicio.despacho')->with("a",1);
    }
    public function pantalla_2(){
        return view('inicio.despacho')->with("a",2);
    }
    public function pantalla_3(){
        return view('inicio.despacho')->with("a",3);
    }
    public function seccion(){
    	return view('configuracion.secciones');

    }
    public function subSeccion(){
    	return view('configuracion.subseccion');

    }
     public function informacion(){
    	return view('contenido.informacion');

    }
}
