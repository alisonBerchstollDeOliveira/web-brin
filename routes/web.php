<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','configuracionController@pantalla_1');
Route::get('/trading','configuracionController@pantalla_2');
Route::get('/juridico','configuracionController@pantalla_3');
Route::get('/config/seccion','configuracionController@seccion');
Route::get('/config/subseccion','configuracionController@subSeccion');
Route::get('/informacion','configuracionController@informacion');




